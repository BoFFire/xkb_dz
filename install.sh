#!/bin/bash

# XKB keyboard layout for X11 - Algeria
# https://gitlab.com/BoFFire/xkb_dz
echo "XKB keyboard layout for X11 - Algeria"

# Function to check and install git if not installed
check_install_git() {
    if ! command -v git &> /dev/null; then
        echo "Git is not installed. Installing git..."
        sudo apt-get update
        sudo apt-get install -y git || { echo "Error: Failed to install git."; exit 1; }
    fi
}

# Function to clone repository if not already exists
clone_repository() {
    if [ ! -d "xkb_dz" ]; then
        git clone https://gitlab.com/BoFFire/xkb_dz.git || { echo "Error: Failed to clone repository."; exit 1; }
    else
        echo "The 'xkb_dz' directory already exists."
    fi
}

# Function to make install script executable
make_install_script_executable() {
    chmod +x install.sh || { echo "Error: Failed to make install script executable."; exit 1; }
}

# Function to backup files
backup_files() {
    echo "Creating backup..."
    sudo cp -r /usr/share/X11/xkb/rules /usr/share/X11/xkb/rules_backup
    sudo cp -r /usr/share/X11/xkb/symbols /usr/share/X11/xkb/symbols_backup
}

# Function to restore files from backup
restore_files() {
    echo "Restoring files from backup..."
    sudo cp -r /usr/share/X11/xkb/rules_backup /usr/share/X11/xkb/rules
    sudo cp -r /usr/share/X11/xkb/symbols_backup /usr/share/X11/xkb/symbols
}

# Function to execute installation script
execute_install_script() {
    ./install.sh || { echo "Error: Failed to execute install script."; restore_files; exit 1; }
}

# Function to prompt user for restoration or proceed
prompt_restore_or_proceed() {
    read -rp "Backup files found. The backup files are located in /usr/share/X11/xkb/rules_backup and /usr/share/X11/xkb/symbols_backup. Do you want to proceed with installation and exit? (y/n): " choice
    case "$choice" in
        [yY])
            echo "Proceeding with installation and exiting."
            execute_install_script
            echo "Setup completed successfully."
            exit 0
            ;;
        [nN])
            echo "Exiting script."
            exit 0
            ;;
        *)
            echo "Invalid choice. Please enter 'y' to proceed with installation and exit or 'n' to exit script."
            prompt_restore_or_proceed
            ;;
    esac
}

# Function to check if backups exist
check_backups() {
    if [[ -d "/usr/share/X11/xkb/rules_backup" && -d "/usr/share/X11/xkb/symbols_backup" ]]; then
        prompt_restore_or_proceed
    else
        echo "No backup files found. Proceeding with installation."
    fi
}

# Main function to orchestrate the setup process
main() {
    check_install_git
    clone_repository
    cd xkb_dz || { echo "Error: Failed to change directory."; exit 1; }
    make_install_script_executable
    check_backups
    backup_files
    execute_install_script
    echo "Setup completed successfully."
    exit 0
}

# Execute the main function
main
